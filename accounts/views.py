from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import (
    authenticate,
    login as auth_login,
    logout as auth_logout,
)
from django.contrib import messages
from .forms import SignUpForm


def login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect("home")
        else:
            messages.error(request, "Invalid username or password")
            return render(request, "accounts/login.html")
    else:
        return render(request, "accounts/login.html")


def logout_view(request):
    auth_logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password != password_confirmation:
                error_message = "The passwords do not match"
                return render(
                    request,
                    "registration/signup.html",
                    {"form": form, "error_message": error_message},
                )
            user = User.objects.create_user(
                username=username, password=password
            )
            auth_login(request, user)
            return redirect("list_projects")
    else:
        form = SignUpForm()
    return render(request, "registration/signup.html", {"form": form})
