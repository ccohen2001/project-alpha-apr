from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from projects.forms import ProjectForm


def project_list(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(owner=request.user)
        context = {"projects": projects}
        return render(request, "project_list.html", context)
    else:
        return redirect("login")


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "project_detail.html", {"project": project})


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    return render(request, "create_project.html", {"form": form})
